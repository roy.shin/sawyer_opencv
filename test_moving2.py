#!/usr/bin/env python

import rospy
import intera_interface

rospy.init_node('Hello_Sawyer')
limb = intera_interface.Limb('right')

#limb.move_to_neutral()


point1 ={'right_j6': 0.0, 'right_j5': -0.9, 'right_j4': 0.0, 'right_j3': 1.8, 'right_j2': -0.0, 'right_j1': -0.9, 'right_j0': -0.4}

#point2 ={'right_j6': 0.0, 'right_j5': -0.9, 'right_j4': 0.0, 'right_j3': 1.8, 'right_j2': -0.0, 'right_j1': -0.9, 'right_j0': 0.4}


limb.move_to_joint_positions(point1)
rospy.sleep(0.5)

#limb.move_to_joint_positions(point2)
#rospy.sleep(0.5)

quit()
