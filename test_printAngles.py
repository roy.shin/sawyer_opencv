#!/usr/bin/env python

import rospy
import intera_interface

rospy.init_node('Hello_Sawyer')
limb = intera_interface.Limb('right')
angles = limb.joint_angles()

print(angles)
