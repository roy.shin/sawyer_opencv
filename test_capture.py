#!/usr/bin/env python

#capturing color and gray 

import cv2

capture = cv2.VideoCapture(1)

while True:
    has_frame, frame = capture.read()
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    if not has_frame:
        print('Can\'t get frame')
        break
    
    cv2.imshow('frame', frame)
    cv2.imshow('gray_frame', gray_frame)
    key = cv2.waitKey(3)
    if key == 27:
        print('Pressed ESC')
        break

capture.release()
cv2.destroyAllWindows()
