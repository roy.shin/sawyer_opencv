#!/usr/bin/env python

import rospy
import intera_interface

rospy.init_node('Hello_Sawyer')
limb = intera_interface.Limb('right')

limb.move_to_neutral()


point1 ={'right_j6': 0.0008671875, 'right_j5': -0.8998486328125, 'right_j4': 0.0027451171875, 'right_j3': 1.799091796875, 'right_j2': -0.0009345703125, 'right_j1': -0.8999853515625, 'right_j0': -0.00046484375}
point2 = {'right_j6': 0.0, 'right_j5': 0.0, 'right_j4': 0.0, 'right_j3': 0.0, 'right_j2': 0.0, 'right_j1': -0.0, 'right_j0': -0.0}


limb.move_to_joint_positions(point2)
rospy.sleep(0.5)

limb.move_to_joint_positions(point1)
rospy.sleep(0.5)

quit()
