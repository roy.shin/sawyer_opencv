#!/usr/bin/env python

#capturing color and gray 

import cv2
import numpy as np

capture = cv2.VideoCapture(1)

while True:
    has_frame, frame = capture.read()
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    circles = cv2.HoughCircles(gray_frame, cv2.HOUGH_GRADIENT, 1, 70, param1 = 190, param2 = 50)

    circles = np.uint16(np.around(circles))

    for i in circles[0, :]:
        cv2.circle(gray_frame, (i[0], i[1]), i[2], (255,255,0), 1)



    if not has_frame:
        print('Can\'t get frame')
        break
    
    cv2.imshow('frame', gray_frame)
    
    
    key = cv2.waitKey(3)
    if key == 27:
        print('Pressed ESC')
        break

capture.release()
cv2.destroyAllWindows()
