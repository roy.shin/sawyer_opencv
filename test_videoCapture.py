#!/usr/bin/env python

import cv2
cam = cv2.VideoCapture(0)
while(cam.isOpend()):
	ret, frame = cam.read()
	cv2.imshow('frame', frame)
	
	k = cv2.waitKey(0) & 0xFF
	
	if k==27:
		break
		
cam.release()
cv2.destroyAllWindows()
