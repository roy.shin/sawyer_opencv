#!/usr/bin/env python
import cv2
import numpy as np 

src = cv2.imread('circle1.png')
gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
ret, bImage = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)

M = cv2.moments(bImage, True)
for key, value in M.items():
	print('{} = {}'.format(key, value))

cx = int(M['m10'] / M['m00'])
cy = int(M['m01'] /M['m00'])
dst = src.copy()
cv2.circle(dst, (cx,cy), 5, (0,0,255), 2)

print('({}, {})'.format(cx, cy))

cv2.imshow('dst', dst)
cv2.waitKey()
cv2.destroyAllWindows()